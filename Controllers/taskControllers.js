const Task = require("../Models/task.js");


/*Controllers and functions*/

// Controller/function to get all the tasks of our database
module.exports.getAll = (request, response) => {

	Task.find({})
	// To capture the result of the find method
	.then(result => {
		return response.send(result);
	})

	// Captures error when the find method is executed
	.catch(error => {
		return response.send(error);
	})
};

// Controller for creating/adding a task to the database

module.exports.createTask = (request, response) => {

	const input = request.body;

	Task.findOne({name: input.name})
	.then(result => {

		if(result !== null){
			return response.send("The task already exists!");
		} else {
			let newTask = new Task({
				name: input.name
			});

			newTask.save().then(save => {
				return response.send(`The task has been added.`);
			})
			// This .catch is for newTask.save
			.catch(error => {
				return response.send(error);
			})
		}
	})
	// This .catch is for the Task.findOne
	.catch(error => {
		return response.send(error);
	})
};

// Controller for deleting tasks from the database

module.exports.deleteTask = (request, response) => {

	let idToBeDeleted = request.params.id;

	//findByIdAndRemove : finds the document with the specified id and then deletes it

	Task.findByIdAndRemove(idToBeDeleted)
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	});
};

// Controller for finding a specific task

module.exports.getOne = (request, response) => {

	let searchTarget = request.params.id;

	Task.findById(searchTarget)
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	});
};

// Controller for updating a specific task

module.exports.updateTask = (request, response) => {

	let updateTarget = request.params.id;

	Task.findByIdAndUpdate(updateTarget, {status: "completed"}, {new: true})
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})
};