const express = require('express');
const mongoose = require('mongoose');

const taskRoute = require("./Routes/taskRoute.js");

// Server set-up
const app = express();
const port = 3001;


// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin@batch245-nava.y30w9yr.mongodb.net/s35-discussion?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
});


// Check connection

let db = mongoose.connection;

// Error catcher
db.on("error", console.error.bind(console, "Connection Error."));

// Connection confirmation
db.once("open", () => console.log("We are now connected to the cloud."));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Routing
// This sets up the routes for the /tasks
app.use("/tasks", taskRoute);







app.listen(port, () =>console.log(`Server is running at port ${port}.`));

/*
	Separation of concerns:

		1. Model should be connected to the controller.
		2. Controller should be connected to the Routes.
		3. Route should be connected to the server/application.

	Server/App > Routes > Controllers > Models

*/